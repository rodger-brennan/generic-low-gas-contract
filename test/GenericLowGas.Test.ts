// Comment out the following line in hardhat.config.ts before running unit tests
// otherwise the console will noise messages '[Ethernal] plugin is not initialized yet' 
// require('hardhat-ethernal');

const { upgrades, ethers } = require("hardhat");
import { BigNumber, Contract } from "ethers";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import { getAddress } from "ethers/lib/utils";
chai.use(chaiAsPromised);
const expect = chai.expect;

describe("GenericLowGas", () => {
    let sut: Contract;
    let owner: SignerWithAddress;
    let address1: SignerWithAddress;
    let address2: SignerWithAddress;
    let payee1: SignerWithAddress;
    let payee2: SignerWithAddress;
    let payee3: SignerWithAddress;

    beforeEach(async () => {
        [owner, address1, address2, payee1, payee2, payee3] = await ethers.getSigners();

        const MyContractFactory = await ethers.getContractFactory("GenericLowGasContract", owner);
        
        sut = await MyContractFactory.deploy('Generic Low Gas', 'GLG', 'https://localhost:8081/');
        
        await sut.deployed();
        console.log("CONTRACT DEPLOYED");
    });

    it("Should set the right owner", async () => {
        expect(await sut.owner()).to.equal(await owner.address);
    });

    it("should return its baseUri", async () => {
        await sut.setBaseURI("https://localhost:8081/")
        expect(await sut.baseURI()).to.equal("https://localhost:8081/");
    });
    
    it("Should not mint when eth sent is less that price", async () => {
        await sut.flipPauseMode();
        
        const mint = sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.0075") });
        await expect(mint).eventually.to.rejectedWith(Error, "VM Exception while processing transaction: reverted with reason string 'Balance Insufficient'");
    });

    it("Should not mint when count is less than one", async () => {
        await sut.flipPauseMode();
        
        const mint = sut.connect(address1).mint(0, { value: ethers.utils.parseEther("0.0075") });
        await expect(mint).eventually.to.rejectedWith(Error, "VM Exception while processing transaction: reverted with reason string 'Select at least 1 NFT'");
    });

    it("Should not charge the owner to mint", async () => {
        await sut.flipPauseMode();        
        
        expect(await sut.connect(owner).mint(1, )).to.emit(
            sut,
            "Transfer"
        );

    })

    it("Should reserve 30 crabs when reserve is called", async () => {
        await sut.connect(owner).reserve();
        
        expect(await sut.connect(owner).totalSupply()).to.equal(30);
    });

    it("Should pause when the the pause function is called", async () => {
        await sut.connect(owner).flipPauseMode(); //start out paused so unpause
        await sut.connect(owner).flipPauseMode(); //unpause
        
        const mint = sut.connect(address1).mint(1, { value: ethers.utils.parseEther("7.5") });
        await expect(mint).eventually.to.rejectedWith(Error, "VM Exception while processing transaction: reverted with reason string 'Paused'");
    });

    it("Should unpause when the the unpause function is called", async () => {
        const mint = sut.connect(address1).mint(1, { value: ethers.utils.parseEther("7.5") });
        await expect(mint).eventually.to.rejectedWith(Error, "VM Exception while processing transaction: reverted with reason string 'Paused'");
        await sut.connect(owner).flipPauseMode();
        
        expect(await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.025") })).to.emit(
            sut,
            "Transfer"
        );

    });

    it("Should not mint more than the per transaction limit", async () => {
        await sut.connect(owner).flipPauseMode();
        
        const mint = sut.connect(address1).mint(11, { value: ethers.utils.parseEther("0.275") });
        await expect(mint).eventually.to.rejectedWith(Error, "VM Exception while processing transaction: reverted with reason string 'Max Mint Amount Reached'");
    });

    it("Should increment totalSupply by mint amount", async () => {
        await sut.connect(owner).flipPauseMode();
        
        const mint = sut.connect(address1).mint(5, { value: ethers.utils.parseEther("0.125") });
        const totalSupply = await sut.connect(owner).totalSupply();
        
        expect(totalSupply).to.equal(5);
    });

    it("Should assign token to the minting address on mint", async () => {
        await sut.connect(owner).flipPauseMode();
        await sut.connect(owner).reserve(); //reserve some tokens 
        
        const mint = await sut.connect(address1).mint(5, { value: ethers.utils.parseEther("0.125") });
        const token30owner = await sut.connect(owner).ownerOf(30);
        const token31owner = await sut.connect(owner).ownerOf(31);
        const token32owner = await sut.connect(owner).ownerOf(32);
        const token33owner = await sut.connect(owner).ownerOf(33);
        const token34owner = await sut.connect(owner).ownerOf(34);
        
        expect(token30owner).to.equal(address1.address);
        expect(token31owner).to.equal(address1.address);
        expect(token32owner).to.equal(address1.address);
        expect(token33owner).to.equal(address1.address);
        expect(token34owner).to.equal(address1.address);
    });

    it("Should not mint a negative amount of tokens", async () => {
        
        const mint = sut.connect(address1).mint(-1, { value: ethers.utils.parseEther("7.5") });
        await expect(mint).eventually.to.rejectedWith(Error, "value out-of-bounds (argument=\"_mintAmount\", value=-1, code=INVALID_ARGUMENT, version=abi/5.5.0)");
    });

    it("Should error when non-owner calls pause", async () => {
        
        await expect(sut.connect(address1).flipPauseMode()).eventually.to.rejectedWith(Error, "VM Exception while processing transaction: reverted with reason string 'Ownable: caller is not the owner'");
    });

    it("Should error when non-owner calls withdraw", async () => {
                
        await expect(sut.connect(address1).withdraw()).eventually.to.rejectedWith(Error, "VM Exception while processing transaction: reverted with reason string 'Ownable: caller is not the owner'");
    });

    it("Should increase balance when tokens are minted", async () => {
        await sut.connect(owner).flipPauseMode();
        await sut.connect(owner).reserve();

        await sut.connect(address1).mint(10, { value: ethers.utils.parseEther("0.275") });
        await sut.connect(address2).mint(10, { value: ethers.utils.parseEther("0.275") });
        
        const balance = await sut.provider.getBalance(sut.address);
        await expect(ethers.utils.formatEther(balance)).to.equal("0.55");

    });

    it("Should succeed when owner calls withdrawal", async () => {
        
        await sut.connect(owner).reserve();
        await sut.connect(owner).flipPauseMode();
        await sut.connect(address1).mint(10, { value: ethers.utils.parseEther("0.275") });
        await sut.connect(address2).mint(10, { value: ethers.utils.parseEther("0.275") });

        const balanceBefore = await ethers.provider.getBalance(sut.address);

        const tx = await sut.connect(owner).withdraw();
    
        const balanceAfter = await ethers.provider.getBalance(sut.address);
        expect(balanceAfter.lt(balanceBefore), 'withdrawal worked').to.be.true
    })

    it("Should support ERC165", async () => {
        expect(await sut.connect(owner).supportsInterface(0x01ffc9a7)).to.be.true;
    })

    it("Should support ERC721", async () => {
        expect(await sut.connect(owner).supportsInterface(0x80ac58cd)).to.be.true;
    })

    it("Should support ERC721_Metadata", async () => {
        expect(await sut.connect(owner).supportsInterface(0x5b5e139f)).to.be.true;
    })

    it("Should support ERC721_Enumerable", async () => {
        expect(await sut.connect(owner).supportsInterface(0x780e9d63)).to.be.true;
    })
    
    it("Should return the correct tokenURI for the 42nd token", async() => {
        await sut.connect(owner).flipPauseMode();
        await sut.connect(owner).setBaseURI("https://localhost:8081/");
        await sut.connect(owner).flipIsProxied();
        await sut.connect(owner).reserve();

        await sut.connect(address1).mint(10, { value: ethers.utils.parseEther("0.275") });
        await sut.connect(address2).mint(10, { value: ethers.utils.parseEther("0.275") });

        const tokenUri = await sut.connect(address1).tokenURI(42)

        expect(tokenUri).to.equal("https://localhost:8081/42.json");

    });

    it("Should return nothing if the token hasn't been minted", async() => {
        await expect(sut.connect(address1).tokenURI(42)).eventually.to.rejectedWith(Error, "VM Exception while processing transaction: reverted with reason string 'ERC721Metadata: URI query for nonexistent token'");
    });

    it("Should return token link with json if the proxy is off", async() => {
        await sut.connect(owner).flipPauseMode();
        await sut.connect(owner).setBaseURI("https://localhost:8081/");
        await sut.connect(owner).flipIsProxied();
        await sut.connect(owner).reserve();

        await sut.connect(address1).mint(10, { value: ethers.utils.parseEther("0.275") });
        await sut.connect(address2).mint(10, { value: ethers.utils.parseEther("0.275") });

        const tokenUri = await sut.connect(address1).tokenURI(42)

        expect(tokenUri).to.equal("https://localhost:8081/42.json");
    });

    it("Should return token link without json if the proxy is on", async() => {
        await sut.connect(owner).flipPauseMode();
        await sut.connect(owner).setBaseURI("https://localhost:8081/");
        // await sut.connect(owner).flipIsProxied();
        await sut.connect(owner).reserve();

        await sut.connect(address1).mint(10, { value: ethers.utils.parseEther("0.275") });
        await sut.connect(address2).mint(10, { value: ethers.utils.parseEther("0.275") });

        const tokenUri = await sut.connect(address1).tokenURI(42)

        expect(tokenUri).to.equal("https://localhost:8081/42");
    });


    it("Should send gifts", async() => {
        await sut.connect(owner).flipPauseMode();
        await sut.connect(owner).setBaseURI("https://localhost:8081/");
        
        const giftcount = [1, 1, 2];
        const gitAddresses = ['0x3C44CdDdB6a900fa2b585dd299e03d12FA4293BC', '0x90F79bf6EB2c4f870365E785982E1f101E93b906', '0x15d34AAf54267DB7D7c367839AAf71A00a2C6A65']
        

        expect(await sut.connect(owner).gift(giftcount, gitAddresses)).to.emit(
            sut,
            "Transfer"
        );

    });

    it("Gift Should fail if the mint counts and address counts don't match", async() => {
        await sut.connect(owner).flipPauseMode();
        await sut.connect(owner).setBaseURI("https://localhost:8081/");
        
        const giftcount = [1, 1,];
        const gitAddresses = ['0x3C44CdDdB6a900fa2b585dd299e03d12FA4293BC', '0x90F79bf6EB2c4f870365E785982E1f101E93b906', '0x15d34AAf54267DB7D7c367839AAf71A00a2C6A65']
        
        await expect(sut.connect(owner).gift(giftcount, gitAddresses)).eventually.to.rejectedWith(Error, "VM Exception while processing transaction: reverted with reason string 'Must provide equal quantities and recipients'");
        

    });

    
    // Note: wallet of owner function removed
    // it("return the token owned by and address", async() => {
    //     await sut.connect(owner).flipPauseMode();
    //     await sut.connect(owner).setBaseURI("https://localhost:8081/");

    //     await sut.connect(owner).reserve();

    //     await sut.connect(address1).mint(10, { value: ethers.utils.parseEther("0.275") });
    //     await sut.connect(address2).mint(10, { value: ethers.utils.parseEther("0.275") });

    //     const tokens = await sut.connect(address1).walletOfOwner(address1.address);

    //     const TokenResult = [];

    //     for (var i = 0; i < tokens.length; i++) {
    //         TokenResult.push(tokens[i].toString())
    //     }

    //     const expectedResult = ['30', '31', '32','33', '34', '35', '36', '37','38','39'];
          

    //     console.log(TokenResult);
    //     console.log(expectedResult);
        
    //     expect(TokenResult).to.eql(expectedResult);

    // });

    it("Should set price", async() => {
        await sut.connect(owner).flipPauseMode();
        await sut.connect(owner).setBaseURI("https://localhost:8081/");

        await sut.connect(owner).setPrice(ethers.utils.parseEther("0.06"));

        const cost = await sut.Price();
        // console.log(cost.toString());
        const costValue = cost.toString();
        
        expect(costValue).to.equal('60000000000000000');

    });

    it("Should set setmaxMintAmount", async() => {
        await sut.connect(owner).flipPauseMode();
        await sut.connect(owner).setBaseURI("https://localhost:8081/");

        await sut.connect(owner).setmaxMintAmount(20);

        const maxMint = await sut.maxMintPerTrx();
        // console.log(cost.toString());
        const maxMintValue = maxMint.toString();
        
        expect(maxMintValue).to.equal('20');

    });

    it("should return index of token", async() => {
        
        await sut.connect(owner).flipPauseMode();
        
        await sut.connect(owner).reserve();

        await sut.connect(address1).mint(10, { value: ethers.utils.parseEther("0.275") });
        await sut.connect(address2).mint(10, { value: ethers.utils.parseEther("0.275") });

        const index = await sut.tokenByIndex(1);
        expect(index).to.be.equal(1)
    })

    it("should error if index of token is out of bounds", async() => {
        
        await expect(sut.tokenByIndex(1)).eventually.to.rejectedWith(Error, "VM Exception while processing transaction: reverted with reason string 'ERC721Enumerable: global index out of bounds'");
        
    })

    it("should error if index of owners token is out of bounds", async() => {
        
        await expect(sut.tokenOfOwnerByIndex(address1.address, 1)).eventually.to.rejectedWith(Error, "VM Exception while processing transaction: reverted with reason string 'ERC721Enumerable: owner index out of bounds'");
        
    })

    it("should return owner of token by index", async() => {
        await sut.connect(owner).flipPauseMode();
        
        await sut.connect(owner).reserve();

        const ownerAddress = await sut.ownerOf(1);

        expect(ownerAddress).to.be.equal(owner.address) ;
    });

    it("should return the name of the token", async() => {
        const contractName = await sut.name();

        expect(contractName).to.be.equal("Generic Low Gas") ;
    });

    it("should return the symbol of the token", async() => {
        const contractName = await sut.symbol();

        expect(contractName).to.be.equal("GLG") ;
    });


    // LONG RUNNING TESTS

    xit("Should error when the mint request is over supply", async () => {
        await sut.flipPauseMode();
        
        for( var i = 0; i < 499; i++ ) {
            const mint = await sut.connect(address1).mint(10, { value: ethers.utils.parseEther("0.250") });
        } //4990

        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4991
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4992
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4993
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4994
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4995
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4996
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4997
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4998
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4999
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //5000
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //5001
        
        const mint = sut.connect(address2).mint(1, { value: ethers.utils.parseEther("0.250") }); //5002
        
        await expect(mint).eventually.to.rejectedWith(Error, "VM Exception while processing transaction: reverted with reason string 'Not Enough Left'");
    });

    //This test fails during code coverage testing due to timeout.
    //but passes with light testing
    xit("Should mint 5000 not counting 0", async () => {
        await sut.flipPauseMode();
        
        for( var i = 0; i < 499; i++ ) {
            const mint = await sut.connect(address1).mint(10, { value: ethers.utils.parseEther("0.250") });
        }

        let minted = await sut.totalSupply(); //should be 4990
        console.log("totalSupply: " + minted);

        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4991
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4992
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4993
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4994
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4995
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4996
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4997
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4997
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4998
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4999
        await sut.connect(address2).mint(1, { value: ethers.utils.parseEther("0.250") }); //5000
        
        
        minted = await sut.totalSupply();
        console.log("totalSupply: " + minted);
        expect(minted).to.be.equal(5001);

        const tokenOwner = await sut.ownerOf(5000);
        
        expect(tokenOwner).to.be.equal(address2.address);
        
    });

    xit("Gift Should fail if the mint is over supply", async() => {
        await sut.connect(owner).flipPauseMode();
        await sut.connect(owner).setBaseURI("https://localhost:8081/");

        for( var i = 0; i < 499; i++ ) {
            const mint = await sut.connect(address1).mint(10, { value: ethers.utils.parseEther("0.250") });
        } //4990

        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4991
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4992
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4993
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4994
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4995
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4996
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4997
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4998
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4999
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //5000
        
        // would fail here
        // await sut.connect(address2).mint(1, { value: ethers.utils.parseEther("0.250") }); // 5001
        // console.log("should have failed here");
        
        const giftcount = [1, 1, 2];
        const giftAddresses = ['0x3C44CdDdB6a900fa2b585dd299e03d12FA4293BC', '0x90F79bf6EB2c4f870365E785982E1f101E93b906', '0x15d34AAf54267DB7D7c367839AAf71A00a2C6A65']
        
        await expect(sut.connect(owner).gift(giftcount, giftAddresses)).eventually.to.rejectedWith(Error, "VM Exception while processing transaction: reverted with reason string 'Not Enough Left'");
        
    });

    xit("Reserve Should fail if the mint is over supply", async() => {
        await sut.connect(owner).flipPauseMode();
        await sut.connect(owner).setBaseURI("https://localhost:8081/");

        for( var i = 0; i < 499; i++ ) {
            const mint = await sut.connect(address1).mint(10, { value: ethers.utils.parseEther("0.250") });
        } //4990

        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4991
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4992
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4993
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4994
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4995
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4996
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4997
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4998
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //4999
        await sut.connect(address1).mint(1, { value: ethers.utils.parseEther("0.250") }); //5000
        
        // would fail here
        // await sut.connect(address2).mint(1, { value: ethers.utils.parseEther("0.250") }); // 5001
        // console.log("should have failed here");
        
        await expect(sut.connect(owner).reserve()).eventually.to.rejectedWith(Error, "VM Exception while processing transaction: reverted with reason string 'Not Enough Left'");
        
    });

    
})