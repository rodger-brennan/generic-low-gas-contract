/**
 * @type import('hardhat/config').HardhatUserConfig
 */

// Comment this line for unit test, uncomment if testin with Ethernal
// require('hardhat-ethernal');
import { task } from "hardhat/config"; 

import "@nomiclabs/hardhat-waffle";
import 'solidity-coverage';
import "hardhat-gas-reporter";
import "@openzeppelin/hardhat-upgrades";
import Web3 from "web3";
import { ethers, providers, utils, Wallet } from "ethers";
import { getContractAddress } from "@openzeppelin/hardhat-upgrades/dist/utils";


const ownerAddress = '0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266'; // Local Owner Test Address
const buyer1Address = '0x70997970c51812dc3a010c7d01b50e0d17dc79c8'; // Local Buyer Test Address

const deployedContractAddress = '0x5FbDB2315678afecb367f032d93F642f64180aa3';

const contractName = "GenericLowGasContract";

task("simulateLaunch" , "Simulate Relaunch")
.setAction(async (taskArgs:any, hre) => { 
    const [owner, addr1, addr2, addr3, addr4, addr5, 
           addr6, addr7, addr8, addr9, addr10,
           addr11, addr12, addr13, addr14, addr15,
           addr16, addr17, addr18, addr19, addr20] = await hre.ethers.getSigners();

    const MyContract = await hre.ethers.getContractFactory(contractName);
    const contract = await MyContract.attach(deployedContractAddress);

    let totalSupply = await contract.connect(owner).totalSupply();

    console.log("starting contract, total supply = " + totalSupply);

    // get the zero token
    await contract.connect(owner).gift([1], [owner.address]);
    
    totalSupply = await contract.connect(owner).totalSupply();
    console.log("minted token 0, total supply = " + totalSupply);

    await contract.connect(owner).gift([178], [owner.address]);
    totalSupply = await contract.connect(owner).totalSupply();
    console.log("reserved 1 - 178 with gift, total supply = " + totalSupply);
    let tokenOwner = await contract.connect(owner).ownerOf(178);
    console.log("owner of token 178 = " + tokenOwner);
    


    let transferTo = async function transferToken(toAddr:any, tokenId:any) {
        setTimeout(() => {}, 100);
        let transfer = await contract.connect(owner).transferFrom(owner.address,
                                                                  toAddr.address,
                                                                  tokenId);
        console.log(transfer);
        tokenOwner = await contract.connect(owner).ownerOf(tokenId);
        console.log("owner of token " + tokenId + " = " + tokenOwner);
    }

    // airdrop tokens
    
    await transferTo(addr1, 118); 
    await transferTo(addr2, 120); 
    await transferTo(addr3, 121); 
    await transferTo(addr4, 122); 
    await transferTo(addr5, 123); 

    await transferTo(addr6, 124); 
    await transferTo(addr6, 125); 
    await transferTo(addr6, 126); 
    await transferTo(addr6, 127); 
    await transferTo(addr6, 128); 
    await transferTo(addr6, 129); 
    await transferTo(addr6, 130); 
    await transferTo(addr6, 131); 
    await transferTo(addr6, 132); 
    await transferTo(addr6, 133); 

    await transferTo(addr7, 134); 
    await transferTo(addr7, 135); 
    await transferTo(addr7, 136); 
    await transferTo(addr7, 137); 
    
    await transferTo(addr8, 138); 
    await transferTo(addr8, 139); 
    await transferTo(addr8, 140); 

    await transferTo(addr9, 141); 
    await transferTo(addr9, 142); 

    await transferTo(addr10, 143); 
    await transferTo(addr10, 144); 
    await transferTo(addr10, 145); 

    await transferTo(addr11, 146); 
    await transferTo(addr11, 147); 

    await transferTo(addr12, 148); 

    await transferTo(addr13, 149); 

    await transferTo(addr14, 150); 
    await transferTo(addr14, 151); 

    await transferTo(addr15, 152); 

    await transferTo(addr16, 153); 

    await transferTo(addr17, 154); 

    // 155 skipped intentionally

    await transferTo(addr18, 156); 

    await transferTo(addr14, 157); 
    await transferTo(addr14, 158); 

    await transferTo(addr7, 159); 

    await transferTo(addr8, 160); 
    await transferTo(addr8, 161); 
    await transferTo(addr8, 162); 

    // 163 skipped intentionally

    await transferTo(addr19, 164); 
    await transferTo(addr19, 165); 
    await transferTo(addr19, 166); 
    await transferTo(addr19, 167); 
    await transferTo(addr19, 168); 
    await transferTo(addr19, 169); 
    await transferTo(addr19, 170); 
    await transferTo(addr19, 171); 
    await transferTo(addr19, 172); 
    await transferTo(addr19, 173); 
    await transferTo(addr19, 174); 
    await transferTo(addr19, 175); 
    await transferTo(addr19, 176); 
    await transferTo(addr19, 177); 
    await transferTo(addr19, 178); 

    console.log("****** Airdrop Complete ********")

    // unpause
    console.log(await contract.connect(owner).flipPauseMode()); 

    // users mint some nfts
    let count = 1
    let ethValue = count * 0.025
    let mint = await contract.connect(addr1).mint(count, { value: ethers.utils.parseEther(ethValue.toString()) });  
    console.log(mint);
    tokenOwner = await contract.connect(owner).ownerOf(179);
    console.log("owner of token 179 = " + tokenOwner);


    count = 5
    ethValue = count * 0.025
    mint = await contract.connect(addr1).mint(count, { value: ethers.utils.parseEther(ethValue.toString()) });  
    console.log(mint);
    tokenOwner = await contract.connect(owner).ownerOf(180);
    console.log("owner of token 180 = " + tokenOwner);
    tokenOwner = await contract.connect(owner).ownerOf(184);
    console.log("owner of token 184 = " + tokenOwner);
   
})

task("readAllOwners" , "Read All Owners")
.setAction(async (taskArgs:any, hre) => {
    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const account2 = await hre.ethers.getSigner(account);
    const MyContract = await hre.ethers.getContractFactory(contractName);
    const contract = await MyContract.attach(
        deployedContractAddress
    );

    const totalSupply = await contract.connect(account2).totalSupply();

    for( var i=0 ; i < totalSupply; i++ ){
        const tokenOwner = await contract.connect(account2).ownerOf(i);
        console.log(i + ',' + tokenOwner);
        setTimeout(() => {  }, 200);
    }    

})

task("reserveTokens", "reserve tokens")
.setAction(async (taskArgs:any, hre) => {
    const owner = Web3.utils.toChecksumAddress(ownerAddress);
    const ownerSigner = await hre.ethers.getSigner(owner);
    const MyContract = await hre.ethers.getContractFactory(contractName);
    const contract = await MyContract.attach(
        deployedContractAddress
    );
    console.log(await contract.connect(ownerSigner).gift([175], ["0x70997970c51812dc3a010c7d01b50e0d17dc79c8"]))

})

task("readCost", "readCost")
.setAction(async (taskArgs:any, hre) => {
    
    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const account2 = await hre.ethers.getSigner(account);
    const MyContract = await hre.ethers.getContractFactory(contractName);
    const contract = await MyContract.attach(
        deployedContractAddress
    );
        
    console.log(await contract.connect(account2).Price());
});

task("setCost", "setCost")
.setAction(async (taskArgs:any, hre) => {
    
    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const account2 = await hre.ethers.getSigner(account);
    const MyContract = await hre.ethers.getContractFactory(contractName);
    const contract = await MyContract.attach(
        deployedContractAddress
    );
    
    const ethValue = 0.0012;

    const weiValue = 1200000000000000;
    console.log(await contract.connect(account2).setPrice(weiValue));
});


task("setBaseURI", "setBaseURI")
.addParam("baseuri", "The Base URI")
.setAction(async (taskArgs:any, hre) => {
    
    const account = Web3.utils.toChecksumAddress("ownerAddress");
    const account2 = await hre.ethers.getSigner(account);
    const MyContract = await hre.ethers.getContractFactory(contractName);
    
    const contract = await MyContract.attach(
        deployedContractAddress
    );
    console.log(await contract.connect(account2).setBaseURI(taskArgs.baseuri));
});

task("reserve", "Reserve 30 tokens")
.setAction(async (taskArgs:any, hre) => {
    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const account2 = await hre.ethers.getSigner(account);
    const MyContract = await hre.ethers.getContractFactory(contractName);
    
    const contract = await MyContract.attach(
        deployedContractAddress
    );
    
    console.log(await contract.connect(account2).reserve());  
})

task("tokenURI", "getTokenURI")
.addParam("tokenid", "tokenid")
.setAction(async (taskArgs:any, hre) => {
    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const account2 = await hre.ethers.getSigner(account);
    const MyContract = await hre.ethers.getContractFactory(contractName);
    
    const contract = await MyContract.attach(
        deployedContractAddress
    );
    const tokenId = +taskArgs.tokenid;
    console.log(tokenId);
    const result = await contract.connect(account2).tokenURI(tokenId);
    console.log(result);  
})

task("mint", "mint")
.addParam("count", "count")
.setAction(async (taskArgs:any, hre) => {
    const account = Web3.utils.toChecksumAddress(buyer1Address);
    const account2 = await hre.ethers.getSigner(account);
    const MyContract = await hre.ethers.getContractFactory(contractName);

    const contract = await MyContract.attach(
        deployedContractAddress
    );
    
    const count = +taskArgs.count;

    const ethValue = count * 0.025
    console.log(count);
    console.log(await contract.connect(account2).mint(count, { value: ethers.utils.parseEther(ethValue.toString()) }));  

})


task("getOwner", "Get Owner")
.setAction(async (taskArgs:any, hre) => {
    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const account2 = await hre.ethers.getSigner(account);
    const MyContract = await hre.ethers.getContractFactory(contractName);
    const contract = await MyContract.attach(
        deployedContractAddress
    );
    console.log(await contract.connect(account2).owner());  
})

task("getBaseURI", "Get Base URI")
.setAction(async (taskArgs:any, hre) => {
    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const account2 = await hre.ethers.getSigner(account);
    const MyContract = await hre.ethers.getContractFactory(contractName);
    
    const contract = await MyContract.attach(
        deployedContractAddress
    );
    console.log(await contract.connect(account2).baseURI());  
})

task("flipPauseMode", "Flip Pause Mode")
.setAction(async (taskArgs:any, hre) => {
    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const account2 = await hre.ethers.getSigner(account);
    const MyContract = await hre.ethers.getContractFactory(contractName);
    
    const contract = await MyContract.attach(
        deployedContractAddress
    );
    console.log(await contract.connect(account2).flipPauseMode());  
})

task("safeMint", "safeMint")
.setAction(async (taskArgs:any, hre) => {
    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const account2 = await hre.ethers.getSigner(account);
    const toAddress = Web3.utils.toChecksumAddress("0x90f79bf6eb2c4f870365e785982e1f101e93b906");

    const MyContract = await hre.ethers.getContractFactory(contractName);
    
    const contract = await MyContract.attach(
        deployedContractAddress
    );
    console.log(await contract.connect(account2).safeMint(toAddress));  
})

task("_safeMint", "_safeMint")
.setAction(async (taskArgs:any, hre) => {
    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const account2 = await hre.ethers.getSigner(account);
    const toAddress = Web3.utils.toChecksumAddress("0x90f79bf6eb2c4f870365e785982e1f101e93b906");

    const MyContract = await hre.ethers.getContractFactory(contractName);
    
    const contract = await MyContract.attach(
        deployedContractAddress
    );
    console.log(await contract.connect(account2).safeMint(toAddress, 1));  
    console.log(await contract.connect(account2).safeMint(toAddress, 1));  
})


task("getTestNetworkBalances", "getTestNetworkBalances")
.setAction(async (taskArgs:any, hre) => {
    const balances = await hre.network.provider.send("eth_accounts");
    
    console.log(balances);  
})

task("getOwnerBalanceLocalNetwork", "getOwnerBalance")
.setAction(async (taskArgs:any, hre) => {
    const balances = await hre.network.provider.send("eth_getBalance", [ownerAddress]);
    const balanceValue = hre.ethers.utils.formatEther(hre.ethers.BigNumber.from(balances));
    console.log(balanceValue);  
})

task("mint5k", "mint5k")
.setAction(async (taskArgs:any, hre) => {
    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const account2 = await hre.ethers.getSigner(account);
    const MyContract = await hre.ethers.getContractFactory(contractName);

    const contract = await MyContract.attach(
        deployedContractAddress
    );
    
    const count = 1;

    const ethValue = count * 0.025
    
    for( var i = 184; i < 5001; i++){
        setTimeout(() => {}, 150)
        console.log(count);
        console.log(await contract.connect(account2).mint(count, { value: ethers.utils.parseEther(ethValue.toString()) }));  
    }
    
    
})

task("mint2k", "mint52")
.setAction(async (taskArgs:any, hre) => {
    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const account2 = await hre.ethers.getSigner(account);
    const MyContract = await hre.ethers.getContractFactory(contractName);

    const contract = await MyContract.attach(
        deployedContractAddress
    );
    
    const count = 1;

    const ethValue = count * 0.025
    
    for( var i = 184; i < 2001; i++){
        setTimeout(() => {}, 150)
        console.log(count);
        console.log(await contract.connect(account2).mint(count, { value: ethers.utils.parseEther(ethValue.toString()) }));  
    }
    
    
})

task("getContractBalance", "getContractBalance")
.setAction(async (taskArgs:any, hre) => {

    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const account2 = await hre.ethers.getSigner(account);
    const MyContract = await hre.ethers.getContractFactory(contractName);
    
    const contract = await MyContract.attach(
        deployedContractAddress
    );
    
    const balances = await contract.provider.getBalance(contract.address);
    const balanceValue = hre.ethers.utils.formatEther(hre.ethers.BigNumber.from(balances));
    console.log(balanceValue);  
})

task("withdraw", "withdraw")
.setAction(async (taskArgs:any, hre) => {

    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const account2 = await hre.ethers.getSigner(account);
    const MyContract = await hre.ethers.getContractFactory(contractName);
    
    const contract = await MyContract.attach(
        deployedContractAddress
    );
    console.log(await contract.connect(account2).withdraw());
})

task("showPayees", "show Payees")
.setAction(async (taskArgs:any, hre) => {

    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const account2 = await hre.ethers.getSigner(account);
    const MyContract = await hre.ethers.getContractFactory(contractName);
    
    const contract = await MyContract.attach(
        deployedContractAddress
    );
    console.log(await contract.connect(account2).payees());
})

task("releaseAllShares", "release all shares")
.setAction(async (taskArgs:any, hre) => {

    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const ownerAccount = await hre.ethers.getSigner(account);

    const account2String = Web3.utils.toChecksumAddress("0x70997970C51812dc3A010C7d01b50e0d17dc79C8");
    const account2 = await hre.ethers.getSigner(account2String);

    const account3String = Web3.utils.toChecksumAddress("0x90F79bf6EB2c4f870365E785982E1f101E93b906");
    const account3 = await hre.ethers.getSigner(account3String);

    const MyContract = await hre.ethers.getContractFactory(contractName);
    
    const contract = await MyContract.attach(
        deployedContractAddress
    );
    console.log(await contract.callStatic.release(ownerAccount.address));
    console.log(await contract.callStatic.release(account2.address));
    console.log(await contract.callStatic.release(account3.address));
})


task("pullPaymentHolder1", "Pull Payment Holder 1")
.setAction(async (taskArgs:any, hre) => {

    const account = Web3.utils.toChecksumAddress(ownerAddress);
    const ownerAccount = await hre.ethers.getSigner(account);

    const account2String = Web3.utils.toChecksumAddress("0x70997970C51812dc3A010C7d01b50e0d17dc79C8");
    const account2 = await hre.ethers.getSigner(account2String);

    const account3String = Web3.utils.toChecksumAddress("0x90F79bf6EB2c4f870365E785982E1f101E93b906");
    const account3 = await hre.ethers.getSigner(account3String);

    const MyContract = await hre.ethers.getContractFactory(contractName);

    const contract = await MyContract.attach(
        deployedContractAddress
    );
    console.log(await contract.connect(ownerAccount).withdrawPayments(ownerAccount.address));
    
})

// const Private_Key = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" //owner

module.exports = {
  networks: {
      localhost: {
        url: "http://127.0.0.1:8545",
        gasPrice: 71000000000,
        blockGasLimit: 30000000,
        hardfork: "london"
      },
  },
  solidity: {
    version: "0.8.9",
    settings: {
      optimizer: {
        enabled: true,
        runs: 1000,
      }
    }
  },
  paths: {
    sources: "./contracts",
    tests: "./test",
    cache: "./cache",
    artifacts: "./artifacts"
  }
  
};