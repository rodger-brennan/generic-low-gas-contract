const hre = require("hardhat");
const ethernal = require('hardhat-ethernal');
Web3 = require('web3');

async function main() {
    
    const MyContract = await hre.ethers.getContractFactory("GenericLowGasContract");

    const myContract = await MyContract.deploy('Generic Low Gas', 'GSG', 'https://localhost:8081/');                                               

    await hre.ethernal.push({
      name: 'GenericLowGasContract',
      address: myContract.address
    });
  
    console.log("My Contract deployed to:", myContract.address);
  }
  
  main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
  });