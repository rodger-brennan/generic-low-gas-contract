# Generic Low Gas Contract
## Description
This is a generic low gas NTF Solidity Contract

## Installation
npm install
npm test
mpn test:light

beyond that check out the hardhat.config.ts functions and
Ethernal

## Contributing
If you have soemthing to add, let me know, I'm looking for
performance enhancements and more through testing

## Authors and acknowledgment
This contract has been created by twitter: @rodger_brennan
and has used work from Squeebo twitter: @squeebo_nft


## License
This is licenseed under MIT and BSD-3 

## Project status
This project is currently under development and will continue
attempts to improve performance.
