// SPDX-License-Identifier: MIT AND BSD-3-Clause 

pragma solidity >=0.7.0 <0.9.0;

import "./ERC721EnumerableLite.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/finance/PaymentSplitter.sol";


pragma solidity >=0.7.0 <0.9.0;

contract GenericLowGasContract is ERC721EnumerableLite, Ownable, PaymentSplitter {
  using Strings for uint256;

  string public baseURI;
  string public baseExtension = ".json";
  bool public isPauseMode = true;
  uint256 public Price = 0.025 ether;
  uint256 public MaxCollection = 5001;
  uint256 public maxMintPerTrx = 10;
  bool public isProxied = true;

  address[] private addressList = [
    0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266,
    0x70997970C51812dc3A010C7d01b50e0d17dc79C8 
  ];
  uint[] private shareList = [
    95,
    5
  ];

  constructor(string memory _name,
              string memory _symbol,
              string memory _initBaseURI)
    ERC721B(_name, _symbol) 
    PaymentSplitter(addressList, shareList){
    setBaseURI(_initBaseURI);
  }

  function _baseURI() internal view virtual returns (string memory) {
    return baseURI;
  }

  /// #if_succeeds {:msg "totalSupply increase by mint amount"} old(totalSupply()) == totalSupply() - _mintAmount;
  function mint(uint256 _mintAmount) public payable {
    require(!isPauseMode, "Paused");
    uint256 supply = totalSupply();
    require(_mintAmount > 0, "Select at least 1 NFT");
    require(supply + _mintAmount <= MaxCollection, "Not Enough Left");  
      if (msg.sender != owner()) {
        require(_mintAmount <= maxMintPerTrx, "Max Mint Amount Reached");
        require(msg.value >= Price * _mintAmount, "Balance Insufficient"); 
        
    }
    for (uint256 i = 0; i < _mintAmount; ++i) {
      _safeMint(msg.sender, supply + i);
    }
  }

  function tokenURI(uint256 tokenId) 
  public view virtual override returns (string memory)
  {
    require(_exists(tokenId), "ERC721Metadata: URI query for nonexistent token");
    string memory base = _baseURI();
        string memory retURI = "";
        if (isProxied) {
            retURI = bytes(base).length > 0
                     ? string(abi.encodePacked(base, tokenId.toString()))
                     : "";
        } else {
            retURI = bytes(base).length > 0
                     ? string(abi.encodePacked(base, tokenId.toString(),
                       baseExtension))
                    : "";
        }

        return retURI;
  }

  function gift(uint[] calldata quantity,
                address[] calldata recipient)
                external onlyOwner{
    require(quantity.length == recipient.length,
            "Must provide equal quantities and recipients" );

    uint totalQuantity = 0;
    uint256 supply = totalSupply();
    for(uint i = 0; i < quantity.length; ++i){
      totalQuantity += quantity[i];
    }
    require( supply + totalQuantity <= MaxCollection,
             "Not Enough Left" );
    delete totalQuantity;

    for(uint i = 0; i < recipient.length; ++i){
      for(uint j = 0; j < quantity[i]; ++j){
        _safeMint( recipient[i], supply++, "" );
      }
    }
  }

  function reserve() public onlyOwner {
        uint256 i;
        uint256 supply = totalSupply();
        require( supply + 30 <= MaxCollection,
             "Not Enough Left" );
            
        for (i = 0; i < 30; i++) {
            _safeMint(msg.sender, supply++, "");
        }
    }

  function flipPauseMode() public onlyOwner{
    isPauseMode = !isPauseMode;
  }
  
  function setPrice(uint256 _newPrice) public onlyOwner {
    Price = _newPrice;
  }

  function setmaxMintAmount(uint256 _newmaxMintAmount) public onlyOwner {
    maxMintPerTrx = _newmaxMintAmount;
  }

  function setBaseURI(string memory _newBaseURI) public onlyOwner {
    baseURI = _newBaseURI;
  }

  function withdraw() public payable onlyOwner {
    (bool cc, ) = payable(owner()).call{value: address(this).balance}("");
    require(cc);
  }

  function flipIsProxied() public onlyOwner {
        isProxied = !isProxied;
    }
}
